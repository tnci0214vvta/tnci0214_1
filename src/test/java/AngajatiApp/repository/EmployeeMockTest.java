package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.validator.EmployeeValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {

    private EmployeeMock employeeMock;

    @BeforeEach
    public void setup() {

        this.employeeMock = new EmployeeMock();
    }

    //test cu date valide, cnp.length() == 13, lastName.length() == 3
    @Test
    void TC1_addValidEmployee() {

        Employee e1 = new Employee(
                "Anca",
                "Pop",
                "2880908123765",
                DidacticFunction.TEACHER,
                3800.00);

        assertTrue(employeeMock.addEmployee(e1));

        assertEquals(7, this.employeeMock.getEmployeeList().size());

    }

    // nevalid - cnp.length() = 12
    @Test
    void TC2_addANonValidEmployee() {

        Employee e2 = new Employee(
                "Anca",
                "Popa",
                "288090812376",
                DidacticFunction.TEACHER,
                3800.00);

        assertFalse(employeeMock.addEmployee(e2));

        assertEquals(6, this.employeeMock.getEmployeeList().size());
    }

    // nevalid - lastName.length() == 1
    @Test
    void TC3_addANonValidEmployee() {

        Employee e3 = new Employee(
                "Anca",
                "P",
                "288090812376",
                DidacticFunction.TEACHER,
                3800.00);

        assertFalse(employeeMock.addEmployee(e3));

        assertEquals(6, this.employeeMock.getEmployeeList().size());
    }

    // nevalid - lastName.length() = 256
    @Test
    void TC4_addANonValidEmployee() {

        Employee e4 = new Employee(
                "Anca",
                "MoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovan",
                "2880908123765",
                DidacticFunction.TEACHER,
                3800.00);

        assertFalse(employeeMock.addEmployee(e4));

        assertEquals(6, this.employeeMock.getEmployeeList().size());
    }

     // nevalid - lastName.length() = 2
    @Test
    void TC5_addANonValidEmployee() {

        Employee e5 = new Employee(
                "Anca",
                "la",
                "288090812376",
                DidacticFunction.TEACHER,
                3800.00);

        assertFalse(employeeMock.addEmployee(e5));

        assertEquals(6, this.employeeMock.getEmployeeList().size());
    }

    // nevalid - cnp.length() = 14
    @Test
    void TC6_addANonValidEmployee() {

        Employee e6 = new Employee(
                "Anca",
                "Popa",
                "28809081237633",
                DidacticFunction.TEACHER,
                3800.00);

        assertFalse(employeeMock.addEmployee(e6));

        assertEquals(6, this.employeeMock.getEmployeeList().size());
    }

    // valid - lastName.length() = 254
    @Test
    void TC7_addAValidEmployee() {

        Employee e7 = new Employee(
                "Anca",
                "MoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldov",
                "2880908123763",
                DidacticFunction.TEACHER,
                3800.00);

        assertTrue(employeeMock.addEmployee(e7));

        assertEquals(7, this.employeeMock.getEmployeeList().size());
    }

    // valid - lastName.length() = 255
    @Test
    void TC8_addAValidEmployee() {

        Employee e8 = new Employee(
                "Anca",
                "MoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldovanMoldova",
                "2880908123763",
                DidacticFunction.TEACHER,
                3800.00);

        assertTrue(employeeMock.addEmployee(e8));

        assertEquals(7, this.employeeMock.getEmployeeList().size());
    }
}