package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest1 {

    @Test
    void modifyEmployeeFunction_TC1() {

        EmployeeMock em = new EmployeeMock();

        em.modifyEmployeeFunction(em.getEmployeeList().get(5), DidacticFunction.LECTURER);

        assertEquals(DidacticFunction.LECTURER, em.getEmployeeList().get(5).getFunction());

    }

    @Test
    void modifyEmployeeFunction_TC2() {

        EmployeeMock em = new EmployeeMock('a');

        assertThrows(IndexOutOfBoundsException.class,
                () ->  em.modifyEmployeeFunction(em.getEmployeeList().get(0), DidacticFunction.TEACHER));

    }

    @Test
    void modifyEmployeeFunction_TC3() {

        EmployeeMock em = new EmployeeMock();

        Employee e = null;

        em.modifyEmployeeFunction(e, DidacticFunction.TEACHER);

        assertThrows(NullPointerException.class,
                () -> e.getFirstName());

        assertEquals(DidacticFunction.ASISTENT, em.getEmployeeList().get(0).getFunction());
        assertEquals(DidacticFunction.LECTURER, em.getEmployeeList().get(1).getFunction());
        assertEquals(DidacticFunction.LECTURER, em.getEmployeeList().get(2).getFunction());
        assertEquals(DidacticFunction.ASISTENT, em.getEmployeeList().get(3).getFunction());
        assertEquals(DidacticFunction.TEACHER, em.getEmployeeList().get(4).getFunction());
        assertEquals(DidacticFunction.TEACHER, em.getEmployeeList().get(5).getFunction());

    }

    @Test
    void modifyEmployeeFunction_TC4() {

        EmployeeMock em = new EmployeeMock();

        Employee e = new Employee("Morgan", "Freeman", "1234567890876", DidacticFunction.TEACHER,  8500d);

        em.modifyEmployeeFunction(e, DidacticFunction.ASISTENT);

        assertEquals(DidacticFunction.ASISTENT, em.getEmployeeList().get(0).getFunction());
        assertEquals(DidacticFunction.LECTURER, em.getEmployeeList().get(1).getFunction());
        assertEquals(DidacticFunction.LECTURER, em.getEmployeeList().get(2).getFunction());
        assertEquals(DidacticFunction.ASISTENT, em.getEmployeeList().get(3).getFunction());
        assertEquals(DidacticFunction.TEACHER, em.getEmployeeList().get(4).getFunction());
        assertEquals(DidacticFunction.TEACHER, em.getEmployeeList().get(5).getFunction());

        assertEquals(DidacticFunction.TEACHER, e.getFunction());
    }
}