package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.validator.EmployeeException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EmployeeTest {

    private Employee employee;
    private Employee employee1;
    private Employee employee2;
    private Employee employee3;
    private Employee employee4;
    private String data;

    @BeforeEach
    public void setup() {

        this.employee = new Employee();
        this.employee1 = new Employee("Matthew", "Robert", "222333444", DidacticFunction.CONFERENTIAR, 5600.00);
        this.employee2 = new Employee("John", "Jones", "555666777", DidacticFunction.TEACHER, 4000.00);
        this.employee3 = new Employee("Amanda", "Lucas", "888999000", DidacticFunction.LECTURER, 5000.00);
        this.data = "Ionel;Pacuraru;123456789087;ASISTENT;2500;0";
        this.employee4 = null;
    }

    @Test
    @Order(2)
    void getFirstName() {

        assertEquals("", employee.getFirstName());
    }

    @Test
    @Order(3)
    void setFirstName() {
        employee.setFirstName("Bob");
        assertEquals("Bob", employee.getFirstName());
    }

    @Disabled
    @Test
    void getLastName() {

        assertEquals("", employee.getLastName());
    }

    @Test
    @Order(5)
    void setLastName() {

        employee.setLastName("Marley");
        assertEquals("Marley", employee.getLastName());
    }

    @Test
    @Timeout(value = 20, unit = TimeUnit.MILLISECONDS)
    @Order(4)
    void setCnp() throws InterruptedException {

        try {

           TimeUnit.MILLISECONDS.sleep(1);

        } catch(InterruptedException e) {

            e.printStackTrace();
        }

        employee.setCnp("123456789");
        assertEquals("123456789", employee.getCnp());

    }

    @Test
    @Order(7)
    void setCnp_withAssertTimeout() {

        // The following assertion fails with an error message similar to:
        // execution exceeded timeout of 10 ms by 91 ms
        assertTimeout(Duration.ofMillis(10), () -> {

            employee.setCnp("123456789");
            assertEquals("123456789", employee.getCnp());

            Thread.sleep(100);
        });
    }

    @Test
    @Order(1)
    void employeeConstructor() {

        assertAll(

                () -> assertEquals("Matthew", employee1.getFirstName()),
                () -> assertEquals(DidacticFunction.TEACHER, employee2.getFunction()),
                () -> assertEquals(DidacticFunction.LECTURER, employee3.getFunction())

        );
    }

    @ParameterizedTest
    @ValueSource(doubles = {3700.00, 7700.00, 8900.00})
    @Order(8)
    void setSalary(double input) {

        employee.setSalary(input);
        assertEquals(input, employee.getSalary());
    }

    @Test
    @Order(6)
    void getEmployeeFromString() throws EmployeeException {

        assertThrows(EmployeeException.class,
                () -> Employee.getEmployeeFromString(data, 1));
    }

    @Test
    void testConstructor() {

        Employee e = new Employee("Anna", "Cook", "1234444444", DidacticFunction.CONFERENTIAR, 3500.00);
        assertNotNull(e);
    }
}